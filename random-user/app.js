var btn = document.querySelector("#btn")
var fullNameDisp = document.querySelector("#fullname")
var username = document.querySelector("#username")
var email = document.querySelector("#email")
var city = document.querySelector("#city")
var avatar = document.querySelector("#avatar")

btn.addEventListener("click", function(){
  fetch("https://randomuser.me/api/")
    .then(handleErrors)
    .then(parseJSON)
    .then(updateProfile)
    .catch(printError)
})

function handleErrors(request) {
  if(!request.ok){
    throw Error(request.status)
  }
  return request
}

function parseJSON(res){
  console.log(res)
  return res.json().then(function(data){
    return data.results[0]
  })
}
  
function updateProfile(data){
  fullNameDisp.innerText = data.name.first + " " + data.name.last
  username.innerText = data.login.username
  email.innerText = data.email
  city.innerText = data.location.city
  avatar.src = data.picture.medium
}

function printError(err){
  console.log(err)
}