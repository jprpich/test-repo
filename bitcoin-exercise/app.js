

var btn = document.querySelector("button")
var priceDisp = document.querySelector("#price")
var currency = "EUR"



btn.addEventListener("click", function(){
  var req = new XMLHttpRequest();  

  req.onreadystatechange = function() {
      if (req.readyState == 4 && req.status == 200) {
          var data = JSON.parse(req.responseText)
         priceDisp.innerText = data.bpi[currency].rate + " " + currency;
      }
  };



  req.open("GET", "https://api.coindesk.com/v1/bpi/currentprice.json")
  req.send()
})



